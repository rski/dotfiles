ln = ln -sf `pwd`

.PHONY: install-nixos install-user fmt git

news:
	home-manager news --impure --flake .#nixos-desktop

hooks:
	${ln}/.pre-commit .git/hooks/pre-commit

fmt:
	fd .nix -x nixfmt -w 120

nix:
	mkdir -p ~/.config/nix
	${ln}/nix.conf ~/.config/nix/nix.conf

nixos-switch:
	nixos-rebuild switch --flake '.#'

hm-desktop:
	home-manager switch --show-trace --impure --flake .#desktop

hm-nixos-desktop:
	home-manager switch --impure --flake .#nixos-desktop

hm-base:
	home-manager switch --impure --flake .#base

git:
	${ln}/git/gitignore ${HOME}/.gitignore
	${ln}/git/gitconfig ${HOME}/.gitconfig

install-ubuntu:
	sudo install -m644 ubuntu-udev.rules /lib/udev/rules.d/40-rski-monitor-hotplug.rules
	sudo udevadm control --reload
	sudo install -m644 xsession.desktop /usr/share/xsessions/xession.desktop
