(setq gc-cons-threshold 80000000
      gc-cons-percentage 0.6)
(defvar rski-file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)

(require 'package)
(setq package-archives nil)
(package-initialize)

;;; Overwrite the custom-file clobbering. I think I want this?
(defun package--save-selected-packages (&optional value) "Do nothing")

;;; Enables C-x n n to do narrow-to-region
(put 'narrow-to-region 'disabled nil)

(defun rski/visit-config ()
  (interactive)
  (find-file "~/Code/dotfiles/init.el"))
(global-set-key (kbd "C-c v") 'rski/visit-config)

;;; Various config options
(setq user-full-name "Romanos Skiadas"
      user-mail-address "rom.skiad@gmail.com"
      custom-file "~/.emacs-custom.el"
      tags-add-tables nil ;;; Don't ask to keep current tags table when changing dirs
      ring-bell-function 'ignore ;;; shut up
      scroll-conservatively 1000 ;;; scroll one line at a time
      sentence-end-double-space nil ;; when filling, use one space after fullstop
      visible-bell nil
      inhibit-startup-screen t
      create-lockfiles nil;; might be a bad idea but for 99% of the time should be ok
      save-interprogram-paste-before-kill t
      apropos-do-all t
      mouse-yank-at-point t
      require-final-newline t
      load-prefer-newer t
      ediff-window-setup-function 'ediff-setup-windows-plain
      save-place-file (concat user-emacs-directory "places")
      backup-directory-alist `((".*" . ,(concat user-emacs-directory "backups")))
      auto-save-file-name-transforms `((".*" ,(concat user-emacs-directory "backups") t))
      column-number-mode 1
      whitespace-style '(face lines-tail)
      ;;this makes resizing not trunkate the frame size and create gaps underneath it in awesome
      frame-resize-pixelwise t

      ;;; shut the fuck up vc forever and ever
      vc-handled-backends nil

      ;; makes lsp-mode faster
      read-process-output-max (* 1024 1024)
      ;; autsave on compilation
      compilation-ask-about-save nil

      inhibit-startup-echo-area-message t

      find-file-visit-truename t
      ;; stop native comp buffer from popping up
      native-comp-async-report-warnings-errors 'silent

      no-blinking-cursor t
      )

(setq shell-file-name (executable-find "bash"))

;; make C-z do nothing
(defun suspend-frame() (interactive) )

(setq-default tab-width 4
              indent-tabs-mode nil
              cursor-type 'bar
              )

;; compose-mail
(global-unset-key (kbd "C-x m"))
;; set-goal-column. Displays that annoying warning
(global-unset-key (kbd "C-x C-n"))

(load custom-file)

(require 'use-package)

(setq use-package-always-ensure t)
(add-hook 'prog-mode-hook #'bug-reference-prog-mode)
(add-hook 'prog-mode-hook (lambda () (setq show-trailing-whitespace t)))

(use-package diminish)

(use-package flycheck
  :hook ((after-init . global-flycheck-mode)
         (after-init . flycheck-pos-tip-mode))
  :init
  (setq flycheck-global-modes '(not emacs-lisp-mode))
  ;;; The manual says doing this doesn't work, but it does?
  ;;; maybe in the :config statnza it won't work
  (setq flycheck-keymap-prefix (kbd "C-c f"))
  :diminish flycheck-mode
  :config
  (use-package flycheck-pos-tip)
  (put 'flycheck-yang-path 'safe-local-variable #'stringp)
  (defvar flycheck-error-list-format
    `[("File" 20)
      ("Line" 5 flycheck-error-list-entry-< :right-align t)
      ("Col" 3 nil :right-align t)
      ("Level" 8 flycheck-error-list-entry-level-<)
      ("ID" 3 t)
      (,(flycheck-error-list-make-last-column "Message" 'Checker) 0 t)]
    "Table format for the error list.")
  )

(use-package flyspell
  :defer t
  :ensure nil
  :if (executable-find "aspell")
  :hook ((text-mode . flyspell-mode)
         (prog-mode . flyspell-prog-mode))
  :diminish 'flyspell-mode)

(use-package company
  :diminish company-mode
  :hook ((after-init . global-company-mode)
         (company-mode . company-tng-mode))
  :config (define-key company-active-map (kbd "M-.") #'company-complete)
  :init
  (setq company-minimum-prefix-length 3
        company-idle-delay 0
        company-require-match nil
        company-auto-complete nil
        company-selection-wrap-around t
        company-show-numbers t ;; M-n completes
        company-dabbrev-downcase nil)
  (setq company-backends '(company-semantic
                           company-clang
                           company-cmake
                           company-capf
                           company-files
                           (company-dabbrev-code
                            company-gtags
                            company-etags
                            company-keywords)
                           company-dabbrev)))

(use-package ivy
  :init (ivy-mode)
  :diminish ivy-mode
  :custom
  (ivy-height 20 )
  (ivy-use-virtual-buffers t "Add recentf and bookmarks to ivy-switch-buffer")
  (ivy-use-selectable-prompt t)
  :config
  ;;; Required for editing search results with ivy-ag and family
  (use-package wgrep :defer t)

  (use-package counsel
    :init (counsel-mode)
    :diminish counsel-mode
    :custom
    (counsel-yank-pop-separator "\n--\n")
    (counsel-find-file-ignore-regexp "\\`\\." "Hide files with leading dots. This can be toggled with C-c C-a or by typing a dot")
    (ivy-initial-inputs-alist nil)
    (counsel-rg-base-command '("rg" "--max-columns" "500" "--with-filename" "--no-heading" "--line-number" "--color" "never" "%s"))
    :config (use-package smex :defer nil)
    :bind (("M-y" . counsel-yank-pop)
           ("M-o" . counsel-semantic-or-imenu)))

  (use-package swiper
    :bind ("M-i" . swiper))

  (use-package ivy-xref
    :init (setq xref-show-xrefs-function #'ivy-xref-show-xrefs))

  ;;; This requires the fonts to be installed, M-x all-the-icons-install-fonts
  ;;; Also note that only 1 transformer can be active, so this effectively prevents me from using say ivy-rich
  (use-package all-the-icons-ivy
    :init (all-the-icons-ivy-setup))
  )

(use-package markdown-mode
  :defer t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :custom (markdown-command "multimarkdown"))

(use-package lua-mode :defer t)

(use-package web-mode
  :defer t
  :config
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  :custom
  (web-mode-engines-alist '(("django" . "\\.html\\'")))
  (web-mode-enable-auto-closing t)
  (web-mode-enable-css-colorization t))

(use-package yang-mode :defer t
  :bind (:map yang-mode-map
              ("C-c u" . sp-backward-up-sexp)) ;; Take me to your parent. sp is *brilliant*
  :hook (yang-mode . (lambda ()
                       (setq imenu-generic-expression
                             '(("leaf" "leaf \\(.*\\) {" 1)
                               ("container" "container \\(.*\\) {" 1)
                               ("list" "list \\(.*\\) {" 1)
                               ("grouping" "grouping \\(.*\\) {" 1)
                               ("import" "import \\(.*\\) {" 1)
                               )))))

;; also for Jenkinsfiles
(use-package groovy-mode :defer t)

(use-package meson-mode :defer t)

(use-package dumb-jump
  :config (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))

(use-package nix-mode
  :defer t
  :hook (nix-mode . lsp))

(use-package yasnippet
  :ensure t
  :init (yas-global-mode t)
  :diminish yas-minor-mode
  :bind (("C-c r" . yas-insert-snippet))
  )
(use-package yasnippet-snippets :ensure t)
(use-package lsp-mode
  :ensure t
  :bind (("C-c a a" . lsp-execute-code-action))
  :custom
  (lsp-enable-suggest-server-download nil)
  (lsp-headerline-breadcrumb-enable nil)
  (lsp-auto-guess-root t)
  (lsp-prefer-flymake :none)
  (lsp-enable-file-watchers nil)
  (lsp-signature-doc-lines 1)
  (lsp-imenu-sort-methods '(position))
  (lsp-restart 'ignore)
  ;; (lsp-response-timeout 2)
  (lsp-progress-via-spinner nil)
  (lsp-enable-links nil) ;;; I don't really get what this feature gives me tbh
  :config
  (use-package lsp-ivy :ensure t)
  (lsp-register-custom-settings
   '(("gopls.completeUnimported" t t)
     ("gopls.staticcheck" nil nil))))

(use-package sh-script
  :hook ((sh-mode . lsp-mode)))

(use-package lsp-pyright
  :ensure t
  :init (setq lsp-pyright-multi-root nil)
  (setq lsp-pyright-python-executable-cmd "python3")
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp))))


(unless (getenv "GOPATH")
  (setenv "GOPATH" "/home/rski/go"))

(use-package go-ts-mode
  :defer t
  :custom (go-ts-mode-indent-offset 4)
  :hook ((go-ts-mode . (lambda ()
                         (add-hook 'before-save-hook #'lsp-format-buffer)))
         (go-ts-mode . (lambda ()
                         (add-hook 'before-save-hook #'lsp-organize-imports)))
         (go-ts-mode . lsp)
         )
  :mode (( "\\.go\\'" . go-ts-mode)
	 ( "\\go.mod\\'" . go-mod-ts-mod))
  :config
  (defun rski/go-mode-setup ()
    (setq fill-column 100)
    (display-fill-column-indicator-mode t)
    (diminish 'auto-fill-mode))
  (add-hook 'go-ts-mode-hook #'rski/go-mode-setup))

(use-package protobuf-mode :defer t)

(use-package rust-mode
  :defer t
  :hook ((rust-mode . rust-enable-format-on-save)
         (rust-mode . lsp)
         )
  :custom
  (lsp-rust-server 'rust-analyzer)
  (rust-format-show-buffer nil)
  (rust-format-goto-problem nil)
  :config
  (defun rski/rust-play ()
    (interactive)
    (compile (format "rustc %s && %s"
                     (buffer-file-name)
                     (s-chop-suffix ".rs" (buffer-file-name)))))
  (use-package flycheck-rust
    :hook (flycheck-mode . flycheck-rust-setup))
  (use-package cargo-mode
    :bind (:map rust-mode-map
                ("<C-return>" . cargo-mode-test-current-test)))
  )

(add-to-list 'major-mode-remap-alist '(c-mode . c-ts-mode))
(add-to-list 'major-mode-remap-alist '(c++-mode . c++-ts-mode))
(add-to-list 'major-mode-remap-alist
             '(c-or-c++-mode . c-or-c++-ts-mode))

(use-package c-ts-mode
  :defer t
  :ensure nil
  :after smartparens
  :hook ((c++-ts-mode . (lambda () (sp-local-pair 'c++-mode "R\"%(" ")%\"" :wrap "C-c w")))
         (c+++-ts-mode . lsp))
)

(use-package rpm-spec-mode :defer t)

(use-package magit
  :defer t
  :custom
  (magit-bury-buffer-function 'magit-mode-quit-window)
  (magit-log-section-commit-count 20)
  (magit-save-repository-buffers 'dontask)
  ;;; word-wise diffs. This is for the current hunk because all makes magit-status really slow in many big hunks
  ;;; It is quite distracting so disable it.
  ;;; magit-diff-refine-hunk 't
  :bind (("C-c g" . magit-status)
         ("C-c m l" . magit-log-buffer-file)
         ("C-c m b" . magit-blame-addition)
         )
  :config
  (set-face-background 'diff-refine-added "green3")
  (defun magit-push-to-gerrit ()
    (interactive)
    (magit-git-command-topdir "git push origin HEAD:refs/for/master"))
  (transient-append-suffix 'magit-push "m"
    '("G" "Push to gerrit" magit-push-to-gerrit))
  ;; taken from https://tsdh.org/posts/2021-06-21-using-eldoc-with-magit.html
  (defun th/magit-eldoc-for-commit (_callback)
  (let ((commit (magit-commit-at-point)))
    (when commit
      (with-temp-buffer
        (magit-git-insert "show"
                          "--format=format:%an <%ae>, %ar"
                          (format "--stat=%d" (window-width))
                          commit)
        (goto-char (point-min))
        (put-text-property (point-min)
                           (line-end-position)
                           'face 'bold)
        (buffer-string)))))
  (defun th/magit-eldoc-setup ()
  (add-hook 'eldoc-documentation-functions
            #'th/magit-eldoc-for-commit nil t)
  (eldoc-mode 1))
  (add-hook 'magit-status-mode-hook #'th/magit-eldoc-setup)
  (add-hook 'magit-log-mode-hook #'th/magit-eldoc-setup)
  (eldoc-add-command 'magit-next-line)
  (eldoc-add-command 'magit-previous-line))

(use-package magit-delta
  :hook (magit-mode . magit-delta-mode)
  :config (magit-delta-delta-args
  '("--24-bit-color" "always"
    "--features" "magit-delta"
    "--color-only")))

(use-package projectile
  :custom
  (projectile-mode-line
   '(:eval (format " [%s]"
                   (projectile-project-name))))
  (projectile-ignored-project-function (lambda (project-root)
                                         (s-prefix? "/nix" project-root)))
  :init
  (projectile-mode)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (add-to-list 'projectile-globally-ignored-directories "vendor")
  :config
  (use-package counsel-projectile
    :init
    (counsel-projectile-mode)
    :custom
    ;;; this makes things a bit faster, I am not sure what the original value provided except for making things slower due to also
    ;;; checking the projectile ignored files regex, which doesn't matter because I let gitignore deal with that anyway.
    (counsel-projectile-find-file-matcher 'ivy--re-filter)
    :bind ("M-I". counsel-projectile-rg))
  )


(use-package helpful
  :defer t
  ;; FIXME describe-fuction/variable are broken??
  :bind (([remap describe-function] . helpful-callable)
         ([remap describe-variable] . helpful-variable)
         ([remap describe-key] . helpful-key)))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode)

(use-package ws-butler
  :hook ((text-mode prog-mode) . ws-butler-mode)
  :diminish ws-butler-mode)

(use-package hl-todo :init (global-hl-todo-mode))

(set-face-attribute 'default nil :family "FiraCode Nerd Font Mono" :height 100) ;;; fonts
(use-package ligature
  :config
  (global-ligature-mode)
  (ligature-set-ligatures 'prog-mode '( "**" "**/" "*>" "*/" "\\\\" "\\\\\\" "{-" "::"
                                       ":::" ":=" "!!" "!=" "!==" "-}" "----" "-->" "->" "->>"
                                       "-<" "-<<" "-~" "#{" "#[" "##" "###" "####" "#(" "#?" "#_"
                                       "#_(" ".-" ".=" ".." "..<" "..." "?=" ";;" "/*" "/**"
                                       "/=" "/==" "/>" "//" "///" "&&" "||" "||=" "|=" "|>" "^=" "$>"
                                       "++" "+++" "+>" "=:=" "==" "===" "==>" "=>" "=>>" "<="
                                       "=<<" "=/=" ">-" ">=" ">=>" ">>" ">>-" ">>=" ">>>" "<*"
                                       "<*>" "<|" "<|>" "<$" "<$>" "<!--" "<-" "<--" "<->" "<+"
                                       "<+>" "<=" "<==" "<=>" "<=<" "<>" "<<" "<<-" "<<=" "<<<"
                                       "<~" "<~~" "</" "</>" "~@" "~-" "~>" "~~" "~~>" "%%")))
(use-package monokai-theme :defer t)
(use-package atom-one-dark-theme :defer t)
(use-package solarized-theme :defer t)
(use-package doom-themes :defer t)

;; move it out of the use-packages so that the diffs won't be as weird when I change themes
(load-theme 'doom-one 'no-confirm)

(use-package doom-modeline
  :ensure nil
  :custom (doom-modeline-minor-modes t)
  :hook (after-init . doom-modeline-mode))


(use-package whitespace
  :defer t
  :ensure nil
  :config
  (set-face-foreground 'whitespace-line "#d33682")
  (set-face-underline 'whitespace-line "#d33682"))

;;; left fringe arrow face (breakpoint triangle)
(defface right-triangle-face
  '((t :foreground "red"))
  "Face for the right-triangle bitmap.")
(set-fringe-bitmap-face 'right-triangle 'right-triangle-face)

(use-package rainbow-delimiters :defer t
  :hook (emacs-lisp-mode . rainbow-delimiters-mode))

(use-package org
  :defer t
  :ensure nil
  :init
  (setq org-todo-keywords
        '((sequence "TODO" "|" "DONE" "ABANDONED")))
  (setq org-hide-leading-stars t)
  ;; in long folding buffers, show-paren-mode + display-line-numbers causes the test to move around
  (defun rski/maybe-disable-show-paren-mode ()
    (if (> (point-max) 100)
        (setq-local show-paren-mode nil)))
  (add-hook 'org-mode-hook
            #'rski/maybe-disable-show-paren-mode)
  :bind (("\C-col" . org-store-link)
         ("\C-coa" . org-agenda)
         ("\C-coc" . org-capture)
         ("\C-cob" . org-switchb))
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (octave . t)))
  (setq org-directory (expand-file-name "~/org"))
  (setq org-agenda-files
        '("~/org/todo.org" "~/org/arista.org" "~/org/buy.org" "~/org/daily.org" "~/org/learning.org" "~/org/agenda.org"))
  (setq org-tag-alist '((:startgroup . nil) ("@work" . ?w) ("@home" . ?h) (:endgroup . nil)
                        (:startgroup . nil) ("@read" . ?r) ("@unread" . ?u) (:endgroup . nil)
                        ))
  (setq org-default-notes-file (concat org-directory "/agenda.org"))
  (setq org-src-fontify-natively t)
  )

(defun rski/indent-buffer()
  "what the fn name says"
  (interactive)
  (indent-region (point-min) (point-max)))

(use-package editorconfig
  :diminish editorconfig-mode
  :config (editorconfig-mode 1))

(use-package smartparens
  :hook ((prog-mode text-mode) . smartparens-mode)
  :init (require 'smartparens-config)
  :diminish smartparens-mode)

;;;Rebind M-; to comment out lines instead of insert comments in the end
(global-set-key (kbd "M-;") 'comment-line)

;;; modeline
(setq rski/hostname (system-name))
(setq-default mode-line-format '("" current-input-method-title mode-line-modified mode-line-remote
                                 mode-line-buffer-identification " "
                                 mode-line-position mode-line-modes mode-line-misc-info rski/hostname))

(use-package eldoc :diminish eldoc-mode :ensure nil)

;;; line numbers
(setq display-line-numbers-grow-only t) ;; confusing otherwise
(global-display-line-numbers-mode 1)

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(horizontal-scroll-bar-mode -1)
(save-place-mode 1)
(show-paren-mode 1)

(defalias 'yes-or-no-p 'y-or-n-p)
(add-to-list 'auto-mode-alist '("Cask" . emacs-lisp-mode))
(add-to-list 'auto-mode-alist '(".g4" . antlr-mode))

(use-package uniquify
  :ensure nil
  :custom
  (uniquify-buffer-name-style 'forward)
  (uniquify-min-dir-content 1))
(global-set-key (kbd "M-/") 'hippie-expand)

(use-package ediff
  :defer t
  :ensure nil
  :init
  (defun rski/ediff-wordwise-in-current-buffer ()
    "Thin wrapper around `ediff-regions-wordwise'.
I always end up doing it in current buffer so might as well wrap it."
    (interactive)
    (ediff-regions-wordwise (current-buffer) (current-buffer))))

(use-package comint
  :defer t
  :ensure nil
  :config
  (set-face-attribute 'comint-highlight-input nil :underline "light gray" :weight 'bold)
  (set-face-attribute 'comint-highlight-prompt nil :foreground "deep sky blue"))

(use-package tex-mode
  :defer t
  :ensure nil
  :custom (tex-dvi-view-command "evince"))

(use-package simple
  :defer t
  ;;; I had wanted to bind these, but magit binds them to go up/down
  ;;; historic commit messages and I do use that. Maybe if I just bind them to prog mode or something.
  ;;; There always is M-g M-n
  ;;; :bind (("M-n" . next-error) ("M-p" . previous-error))
  :init (defun rski/delete-hz-space ()
          (interactive)
          (delete-horizontal-space)
          (insert " "))
  :bind (("M-\\" . rski/delete-hz-space))
  :ensure nil)

(use-package eshell
  :ensure nil
  :defer t
  :bind ("C-c e" . eshell)
  :custom (eshell-banner-message "Eshell, because the existing shells were not bad enough already.\n\n")
  :hook (eshell-mode . (lambda ()
                         (company-mode -1)
                         ;;; by abo-abo @https://emacs.stackexchange.com/questions/27849/how-can-i-setup-eshell-to-use-ivy-for-tab-completion
                         (define-key eshell-mode-map (kbd "<tab>") 'completion-at-point))))

(use-package with-editor
  :hook ((eshell-mode . with-editor-export-editor)
         (eshell-mode . with-editor-export-git-editor)))

(use-package marginalia
  :config (marginalia-mode))

;; (use-package apheleia)  for some reason this gets installed at the top level site-packages dir.
;; It's not like I use it anyway.

(use-package autorevert
  :ensure nil
  :config (global-auto-revert-mode))

(setq battery-echo-area-format "%L %B (%p%% %t)")

;; shut up shut up shut up shut up
(eval-after-load "flyspell"
  '(define-key flyspell-mode-map (kbd "C-.") nil))
(eval-after-load "flyspell"
  '(define-key flyspell-mode-map (kbd "C-,") nil))

(setq gc-cons-threshold 800001
      gc-cons-percentage 0.1
      file-name-handler-alist rski-file-name-handler-alist)


;;; pressing "a" in dired visits the thing (directory/file) and kills the previous buffer.
;;; Much better than pressing RET and leaving buffers
(put 'dired-find-alternate-file 'disabled nil)

(setq xref-after-jump-hook '(recenter))
(setq xref-after-return-hook '())
(let ((arista-settings (format "%s/arista.el" user-emacs-directory)))
 (when (file-exists-p arista-settings)
   (load-file arista-settings)))

(use-package keychain-environment
  :config (keychain-refresh-environment))

(use-package clipetty
  :ensure t
  :bind ("M-w" . clipetty-kill-ring-save))

(use-package dtrt-indent
  :ensure t
  :config (dtrt-indent-global-mode))
