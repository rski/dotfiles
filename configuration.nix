# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ pkgs, options, ... }:

{
  # Include the results of the hardware scan.
  imports = [ ./hardware-configuration.nix ];

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    initrd.kernelModules = [ "i915" ];
    loader.grub = {
      device = "/dev/sda";
      enable = true;
      efiSupport = true;
    };
  };

  programs.dconf.enable = true;
  programs.steam.enable = true;
  programs.fish.enable = true;
  programs.ssh.askPassword = pkgs.lib.mkForce "";

  networking = {
    hostName = "neonauticus";
    firewall.allowedTCPPorts = [ 8123 ];
    networkmanager = {
      insertNameservers = [ "127.0.0.1" ];
      enable = true;
      wifi.powersave = false;
    };
  };

  i18n.defaultLocale = "en_GB.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  time.timeZone = "Europe/Athens";

  nixpkgs.config.allowUnfree = true;

  # Enable sound.
  sound.enable = true;
  hardware = {
    bluetooth = {
      enable = true;
      settings.General.Experimental = true;
      package = pkgs.bluez5-experimental;
    };
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
      extraPackages = with pkgs; [
        vaapiIntel
        vaapiVdpau
        libvdpau-va-gl
        intel-media-driver
      ];
    };
  };

  services = {
    adguardhome = {
      enable = true;
      settings = {
        bind_host = "127.0.0.1";
        bind_port = 8124;
        dns = {
          bind_host = "127.0.0.1";
          bootstrap_dns = [
            "9.9.9.10"
            "149.112.112.10"
            "2620:fe::10"
            "2620:he::fe:10"
          ];
          upstream_dns = [
            "1.0.0.1"
            "1.1.1.1"
            "8.8.8.8"
          ];
        };
      };
    };
    blueman.enable = true;
    gnome.gnome-keyring.enable = true;
    udisks2.enable = true;
    gvfs.enable = true;
    earlyoom = {
      enable = true;
      freeMemThreshold = 5;
      enableNotifications = true;
    };
    thermald.enable = true;
    upower.enable = true;
    plex = {
      enable = true;
      openFirewall = true;
    };
    xserver = {
      enable = true;
      # Enable touchpad support.
      libinput = {
        enable = true;
        mouse.accelProfile = "flat";
      };
      desktopManager.plasma5 = {
        enable = true;
      };
      videoDrivers = [ "intel" ];
      extraConfig = ''
        Section "Device"
          Identifier "Intel Graphics"
          Driver "intel"
            Option      "TearFree"        "false"
            Option      "TripleBuffer"    "false"
            Option      "SwapbuffersWait" "false"
        EndSection
      '';
    };
    pipewire = {
      enable = true;
      audio.enable = true;
      pulse.enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
    };
  };

  environment = {
    systemPackages = with pkgs; [ ntfs3g ];
    defaultPackages = pkgs.lib.mkForce [ ];
    plasma5.excludePackages = [ pkgs.okular ];
    # enableDebugInfo = true;
    etc."xdg/gtk-3.0/settings.ini".text = ''
      [Settings]
      gtk-icon-theme-name=Arc
      gtk-theme-name=Arc
    '';
    homeBinInPath = true;
    variables = {
      EDITOR = "nvim";
      PATH = "$HOME/go/bin";
      # no idea why this is on by default
      CGO_ENABLED = "0";
      # i can turn it off when needed
      GO111MODULE = "on";
      GOPATH = "$HOME/go";
    };
  };

  xdg.mime.enable = true;

  users = {
    users = {
      rski = {
        isNormalUser = true;
        extraGroups = [
          "wheel"
          "networkmanager"
          "docker"
          "video"
          "plex"
        ];
        shell = pkgs.fish;
      };
      root.initialHashedPassword = "";
    };
    mutableUsers = true;
  };

  # enable trim for devices that support it
  systemd.services.fstrim = {
    startAt = "18:00";
    description = "Trim SSD";
    serviceConfig.Type = "oneshot";
    serviceConfig.ExecStart = "${pkgs.util-linux}/bin/fstrim -a -v";
  };
  systemd.tmpfiles.rules = [ "d '/home/shared/Video' - rski plex - -" ];

  nix = {
    settings = {
      trusted-users = [
        "root"
        "rski"
      ];
      sandbox = true;
    };
    extraOptions = ''
      extra-experimental-features = nix-command
      extra-experimental-features = flakes
    '';
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?
}
