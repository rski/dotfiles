{
  pkgs,
  config,
  lib,
  inNixos ? false,
  ...
}:
let
  homeDir = "/home/rski";
  enable = x: x // { enable = true; };
in
{
  home = {
    packages = with pkgs; [
      gopls
      nodePackages.bash-language-server
      pyright
      rust-analyzer

      colordiff
      delta
      fd
      fzf
      git-codereview
      iotop
      procs
      ripgrep
      shellcheck
      sysstat
      tree
      tree-sitter
    ];
    file."bin/scs".source = ../bin/scs;
    file."bin/emacs".source = "${config.programs.emacs.finalPackage}/bin/emacs";
    file."bin/emacsclient".source = "${config.programs.emacs.finalPackage}/bin/emacsclient";
    file.".gdbinit".text = ''
      set print static off
    '';
    username = "rski";
    homeDirectory = homeDir;
    stateVersion = "21.11";
    sessionVariables = {
      EDITOR = "${config.programs.emacs.finalPackage}/bin/emacsclient -c";
      ALTERNATE_EDITOR = "";
      XCURSOR_SIZE = 12;
      GOPATH = "${homeDir}/Code/go";
      OCTEST_NO_WARNINGS = "1";
      # Could not get xterm-direct to work over mosh or ssh even though toe -a lists it
      # https://www.gnu.org/software/emacs/manual/html_node/efaq/Colors-on-a-TTY.html
      COLORTERM = "truecolor";
      GOTOOLCHAIN = "local";
    };
  };
  programs = {
    emacs = enable {
      package = pkgs.emacs-git;
      extraPackages =
        epkgs: with epkgs; [
          (treesit-grammars.with-grammars (
            grammars: with grammars; [
              tree-sitter-bash
              tree-sitter-nix
              tree-sitter-go
              tree-sitter-gomod
              tree-sitter-c
              tree-sitter-cpp
              tree-sitter-dockerfile
              tree-sitter-yaml
            ]
          ))
          all-the-icons-ivy
          atom-one-dark-theme
          cargo-mode
          clipetty
          company
          corfu
          corfu-terminal
          counsel-projectile
          diminish
          doom-modeline
          doom-themes
          dtrt-indent
          dumb-jump
          editorconfig
          eldoc
          flycheck
          flycheck-pos-tip
          flycheck-rust
          groovy-mode
          helpful
          hl-todo
          htmlize
          ivy-xref
          keychain-environment
          ligature
          lsp-ivy
          lsp-mode
          lsp-pyright
          lsp-treemacs

          lua-mode
          magit
          magit-delta
          marginalia
          meson-mode
          monokai-theme
          nix-mode
          projectile
          protobuf-mode
          rainbow-delimiters
          rpm-spec-mode
          rust-mode
          smartparens
          smex
          solarized-theme
          use-package
          web-mode
          wgrep
          which-key
          ws-butler
          yang-mode
          yasnippet
          yasnippet-snippets

          apheleia
        ];
    };
    neovim = {
      enable = true;
      plugins = with pkgs.vimPlugins; [
        nvim-treesitter
        {
          plugin = gruvbox-nvim;
          config = "vim.cmd([[colorscheme gruvbox]])";
          type = "lua";
        }
      ];
    };
    home-manager.enable = true; # let Home Manager install and manage itself.
    zoxide.enable = true;
    htop = enable {
      settings = {
        show_program_path = false;
        hide_userland_threads = true;
        highlight_base_name = 1;
        highlight_megabytes = 1;
        sort_key = config.lib.htop.fields.M_RESIDENT;
        sort_direction = -1;
        fields = with config.lib.htop.fields; [
          PID
          USER
          PRIORITY
          NICE
          M_SIZE
          M_RESIDENT
          M_SHARE
          M_TRS
          M_DRS
          M_LRS
          STATE
          PERCENT_CPU
          PERCENT_MEM
          TIME
          COMM
        ];
      };
    };
    eza = enable { enableFishIntegration = true; };
    bat = enable {
      config = {
        style = "changes";
      };
    };
    tmux = enable {
      escapeTime = 0;
      extraConfig = ''
        set -g status-bg black
        set -g status-fg white
      '';
    };
    fish = enable {
      interactiveShellInit = ''
        set -U fish_greeting "fish, finally the 90s"
      '';
      functions = {
        kc = "keychain --eval --quiet -Q id_rsa_gerrit id_ed25519_gh_aristanetworks | source";
        nixe = "nix-env -e $argv";
      };
      shellAliases = {
        em = "emacsclient -c";
        g = "git";
        gc = "nix-collect-garbage -d";
        nixq = "nix-env -q";
      } // (pkgs.lib.optionalAttrs inNixos { gc = "sudo nix-collect-garbage -d"; });
    };
  };
  xdg = {
    configFile = {
      "emacs/init.el".source = ../init.el;
      "emacs/early-init.el".source = ../early-init.el;
      "cheat/conf.yml".source = ../cheat.yml;
    };
  };
  systemd.user.startServices = "sd-switch";
}
