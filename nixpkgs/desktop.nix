{
  config,
  lib,
  pkgs,
  inNixos ? false,
  ...
}:
let
  nixGL = pkgs.nixgl.nixGLIntel;
  enable = x: x // { enable = true; };
in
{
  home = {
    packages = with pkgs; [
      # desktop stuff
      arandr

      qogir-theme
      qogir-icon-theme

      audacity
      bandwhich
      cachix
      cheat
      chrysalis
      diffoscopeMinimal
      dnsutils
      entr
      eog
      evince
      file
      flamegraph
      gimp
      git
      glxinfo # useful to check if dri is on
      graphviz
      gtypist
      hunspell
      hyperfine
      intel-gpu-tools
      ispell
      jq
      keychain
      lm_sensors
      lshw
      lsof
      lxmenu-data
      mosh
      mtr
      multimarkdown
      neofetch
      nixGL
      nixfmt-rfc-style
      openssl
      pavucontrol
      pcmanfm
      playerctl
      pv
      rsync
      screenkey
      scrot
      sshpass
      strace
      thunderbird
      traceroute
      tokei
      unzip
      vlc
      wget
      nerd-fonts.fira-code
      nerd-fonts.fira-mono
      xorg.xbacklight

      #(lowPrio binutils-unwrapped) # ar, which is used by cargo flycheck or something
      cargo
      cargo-edit
      cargo-binutils
      clang-tools
      gcc
      gdb
      gnumake
      go_1_23
      meson
      ninja
      python3
      clippy
      rustc
      rustfmt
    ];
    file."bin/goimports".source = "${pkgs.gotools}/bin/goimports";
    file.".xprofile".text = ''
      . "${config.home.profileDirectory}/etc/profile.d/hm-session-vars.sh"

      if [ -e "$HOME/.profile" ]; then
        . "$HOME/.profile"
      fi

      #xinput set-prop  "pointer:Razer Razer DeathAdder V2 X HyperSpeed"  "libinput Accel Profile Enabled" 0, 1
      systemctl --user start hm-graphical-session.target

      export HM_XPROFILE_SOURCED=1
    '';
    keyboard = {
      options = [
        "ctrl:nocaps"
        "grp:rctrl_toggle"
      ];
      layout = "us,gr";
    };
  };
  xsession = enable {
    scriptPath = ".xsession-hm";
    profilePath = ".xprofile-hm";
  };
  fonts.fontconfig.enable = true;
  services = {
    xsettingsd = enable {
      settings = {
        "Net/ThemeName" = "Qogir-dark";
        "Net/IconThemeName" = "Qogir-dark";
        "Gtk/ThemeName" = "Qokir-dark";
        "Gtk/IconThemeName" = "Qokir-dark";
        "Gtk/EnableAnimations" = 1;
        "Gtk/DecorationLayout" = "icon:minimize,maximize,close";
        "Gtk/PrimaryButtonWarpsSlider" = 0;
        "Gtk/ToolbarStyle" = 3;
        "Gtk/MenuImages" = 1;
        "Gtk/ButtonImages" = 1;
        "Gtk/CursorThemeSize" = 12;
        "Gtk/FontName" = "Noto Sans,10";
      };
    };
  };
  programs = {
    autorandr = enable {
      hooks.postswitch = {
        "setxkbmap" = "systemctl --user restart setxkbmap.service";
      };
    };
    command-not-found.enable = inNixos;
    rofi = enable {
      terminal = "${pkgs.kitty}/bin/kitty";
      plugins = [ pkgs.rofi-emoji ];
      extraConfig = {
        modi = "drun,run,window,combi,filebrowser,emoji";
        combi-modi = "window,drun,run";
      };
    };

    firefox = enable {
      profiles = {
        default = {
          path = "wisp84ig.default"; # this was the path of the profile when I migrated to HM
          settings = {
            "toolkit.telemetry.pioneer-new-studies-available" = false;
            "general.smoothScroll" = true;
            "media.hardwaremediakeys.enabled" = false;
            "extensions.pocket.enabled" = false;
            "browser.link.open_newwindow.restriction" = 0;
          };
        };
      };
    };

    kitty = enable { extraConfig = builtins.readFile ../kitty.conf; };
  };
  xdg = {
    mime.enable = true;
    mimeApps = enable {
      defaultApplications = {
        "inode/directory" = "pcmanfm.desktop";
        "application/x-extension-htm" = "firefox.desktop";
        "application/x-extension-html" = "firefox.desktop";
        "application/x-extension-shtml" = "firefox.desktop";
        "application/x-extension-xht" = "firefox.desktop";
        "application/x-extension-xhtml" = "firefox.desktop";
        "application/x-m4" = "nvim.desktop";
        "application/pdf" = "org.gnome.Evince.desktop";
        "application/xhtml+xml" = "firefox.desktop";
        "text/html" = "firefox.desktop";
        "video/mp4" = "vlc.desktop";
        "video/x-matroska" = "vlc.desktop";
        "x-scheme-handler/chrome" = "firefox.desktop";
        "x-scheme-handler/http" = "firefox.desktop";
        "x-scheme-handler/https" = "firefox.desktop";
        "application/x-subrip" = "nvim.desktop";
        "x-scheme-handler/onepassword" = "1Password.desktop";
        "text/x-lua" = "nvim.desktop";
        "image/png" = "org.gnome.eog.desktop";
        "image/jpeg" = "org.gnome.eog.desktop";
        "x-scheme-handler/magnet" = "deluge";
      };
    };
    configFile = {
      "kitty/current-theme.conf".source = ../kitty-theme.conf;
      "cheat/cheatsheets/personal".source = ../cheatsheets;
      "cheat/cheatsheets/community".source = pkgs.fetchFromGitHub {
        owner = "cheat";
        repo = "cheatsheets";
        rev = "9006d4ec749ea25b404397ea450cca3d240f52af";
        sha256 = "0lhi4kaj04v7yflpm5m4mjnk779wcpxiwi6i43psavxzmjiraz6r";
      };
    };
  };
  targets.genericLinux.enable = !inNixos;
}
