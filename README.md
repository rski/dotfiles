# Hacks

## Getting xbacklight to work

From https://wiki.archlinux.org/index.php/backlight#xbacklight

Added this to /etc/X11/xorg.conf:

    Section "Device"
        Identifier  "Intel Graphics"
        Driver      "intel"
        Option      "Backlight"  "intel_backlight"
    EndSection
