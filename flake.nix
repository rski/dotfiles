{
  description = "Home Manager configuration of Jane Doe";

  inputs = {
    emacs.url = "github:nix-community/emacs-overlay";
    nixpkgs.follows = "emacs/nixpkgs";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixgl = {
      url = "github:guibou/nixGL";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      nixpkgs,
      emacs,
      home-manager,
      nixgl,
      ...
    }:
    let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        config.allowUnfree = true;
        overlays = [
          emacs.overlays.default
          nixgl.overlay
        ];
      };
    in
    {
      nixosConfigurations.neonauticus = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [ ./configuration.nix ];
      };
      homeConfigurations.desktop = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = [
          ./nixpkgs/home.nix
          ./nixpkgs/desktop.nix
        ];
        extraSpecialArgs.inNixos = false;
      };
      homeConfigurations.nixos-desktop = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = [
          ./nixpkgs/home.nix
          ./nixpkgs/desktop.nix
        ];
        extraSpecialArgs.inNixos = true;
      };
      homeConfigurations.base = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = [
          ./nixpkgs/home.nix
          /home/rski/Code/arista-dotfiles/arista.nix
        ];
        extraSpecialArgs.inNixos = false;
      };
    };
}
