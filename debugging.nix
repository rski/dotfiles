# nix-shell debugging.nix
with import <nixpkgs> { };

[
  (enableDebugging pkgs.emacs)
  (enableDebugging (
    gnucash.overrideAttrs (old: rec {
      name = "mygnucash";
      separateDebugInfo = true;
      meta = old.meta // {
        outputsToInstall = old.meta.outputsToInstall or [ "out" ] ++ [ "debug" ];
      };
    })
  ))
]
